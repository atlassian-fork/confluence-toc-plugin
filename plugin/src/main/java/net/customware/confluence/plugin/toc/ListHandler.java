/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.toc;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * @author David Peterson
 */
public class ListHandler implements OutputHandler
{
    private static final String ALPHA_NUMERIC_STR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final Set<Character> UNENCODED_SET;

    static
    {
        Set<Character> unencodedSet = new HashSet<Character>();
        for (Character c : ALPHA_NUMERIC_STR.toCharArray())
        {
            unencodedSet.add(c);
        }
        UNENCODED_SET = Collections.unmodifiableSet(unencodedSet);
    }

    /**
     * The style to use when no bullet style is desired.
     */
    public static final String NONE_STYLE = "none";

    private static enum HierarchyEvent
    {
        LEVEL,
        HEADING
    }

    private String style;

    private String indent;

    private LinkedList<HierarchyEvent> indentationStack;

    private boolean prefixWritten = false;

    public ListHandler(String style, String indent)
    {
        // Get any parameter values.
        this.style = style;

        // Get list-specific parameters
        this.indent = indent;

        indentationStack = new LinkedList<HierarchyEvent>();
    }

    /**
     * Creates the style block for this TOC, returning the class name generated.
     *
     * @param out The string buffer to output into.
     * @return The unique style class name for this instance.
     */
    @Override
    public String appendStyle(Appendable out) throws IOException
    {
        if (style != null || indent != null)
        {
            String styleClass = "rbtoc" + System.currentTimeMillis();
            out.append("<style type='text/css'>/*<![CDATA[*/\n");

            out.append("div.").append(styleClass).append(" {");
            out.append("padding: 0px;");
            out.append("}\n");

            out.append("div.").append(styleClass).append(" ul {");
            if (style != null)
            {
                out.append("list-style: ").append(encode(style)).append(";");
            }
            out.append("margin-left: 0px;");
            if (indent != null)
            {
                out.append("padding-left: ").append(encode(indent)).append(";");
            }
            out.append("}\n");

            out.append("div.").append(styleClass).append(" li {");
            out.append("margin-left: 0px;");
            out.append("padding-left: 0px;");
            out.append("}\n");

            out.append("\n/*]]>*/</style>");

            return styleClass;
        }
        return null;
    }

    /**
     * This method encodes a String to safely be used in CSS. Copied from http://code.google.com/p/owasp-esapi-java/source/browse/branches/1.4/src/main/java/org/owasp/esapi/codecs/CSSCodec.java
     */
    public String encode(String input)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++)
        {
            char c = input.charAt(i);
            sb.append(encodeCharacter(c));
        }

        return sb.toString();
    }

    /**
     * Encodes a character for use in CSS.
     *
     * @param c The character to encode
     * @return c encoded for use in CSS.
     */
    public String encodeCharacter(Character c)
    {
        char ch = c;

        if (UNENCODED_SET.contains(c))
        {
            return c.toString();
        }

        // CSS 2.1 section 4.1.3: "It is undefined in CSS 2.1
        // what happens if a style sheet does contain a character
        // with Unicode codepoint zero."
        if (ch == 0)
        {
            throw new IllegalArgumentException("Character value zero is not valid in CSS");
        }

        // otherwise encode with \\HHHHHH
        String temp = Integer.toHexString((int) ch);
        return "\\" + temp.toUpperCase() + " ";
    }

    @Override
    public void appendIncLevel(Appendable out) throws IOException
    {
        appendOpenUITag(out);
        indentationStack.push(HierarchyEvent.LEVEL);
    }

    @Override
    public void appendDecLevel(Appendable out) throws IOException
    {
        final HierarchyEvent lastEvent = indentationStack.isEmpty() ? null : indentationStack.pop();
        if (HierarchyEvent.HEADING.equals(lastEvent))
        {
            out.append("</li>");
        }
        out.append("\n</ul>\n");
    }

    @Override
    public void appendPrefix(Appendable out) throws IOException
    {
        prefixWritten = true;
        appendOpenUITag(out);
    }

    private void appendOpenUITag(Appendable out) throws IOException
    {
        out.append("\n<ul class='toc-indentation'>\n");
    }

    @Override
    public void appendPostfix(Appendable out) throws IOException
    {
        if (prefixWritten)
        {
            appendDecLevel(out);
        }
    }

    @Override
    public void appendSeparator(Appendable out)
    {
    }

    @Override
    public void appendHeading(Appendable out, String string) throws IOException
    {
        final HierarchyEvent lastEvent = indentationStack.isEmpty() ? null : indentationStack.pop();
        if (HierarchyEvent.HEADING.equals(lastEvent))
        {
            out.append("</li>\n");
        }
        out.append("<li>");
        out.append(string);
        indentationStack.push(HierarchyEvent.HEADING);
    }

}
