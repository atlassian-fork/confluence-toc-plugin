/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.toc;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.HtmlToXmlConverter;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.customware.confluence.plugin.toc.StaxDocumentOutlineCreator.StaxOutlineBuilderException;

import static com.atlassian.renderer.RenderContextOutputType.PDF;
import static com.atlassian.renderer.RenderContextOutputType.WORD;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * The table of contents macro generates a table of contents for the page based on the headings in the page.
 * 
 * @author David Peterson
 */
public abstract class AbstractTOCMacro extends BaseMacro implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(TOCMacro.class);

    private static final String CLASS_PARAM = "class";

    /**
     * The default depth that the macro will retrieve headings for.
     */
    public static final int DEFAULT_MAX_LEVEL = 7;

    /**
     * The default starting depth for headings.
     */
    public static final int DEFAULT_MIN_LEVEL = 1;

    /**
     * The default style for bullet list
     */
    public static final String DEFAULT_STYLE = "disc";

    private static final String FILTER_PARAM = "filter";

    /**
     * The type value to indicate the toc should be output in 'flat' form.
     */
    public static final String FLAT_TYPE = "flat";

    /**
     * The type value to indicate the toc should be output in list form.
     */
    public static final String LIST_TYPE = "list";

    private static final String MAX_LEVEL_PARAM = "maxLevel";
    private static final String MIN_LEVEL_PARAM = "minLevel";
    private static final String MAX_LEVEL_LOWCASE_PARAM = "maxlevel";
    private static final String MIN_LEVEL_LOWCASE_PARAM = "minlevel";
    private static final String OUTLINE_PARAM = "outline";
    private static final String PRINTABLE_PARAM = "printable";
    private static final String TYPE_PARAM = "type";
    private static final String ABSOLUTE_URL_PARAM = "absoluteUrl";
    private static final String SEPARATOR_PARAM = "separator";
    private static final String INDENT_PARAM = "indent";
    private static final String STYLE_PARAM = "style";

    static final String INCLUDE_PARAM = "include";
    static final String EXCLUDE_PARAM = "exclude";

    private static final ThreadLocal<Boolean> renderingThreadLocal = new ThreadLocal<Boolean>();

    protected static final String PLUGIN_KEY = "org.randombits.confluence.toc";

    protected static final String SERVER_IMPL_WEBRESOURCE_CONTEXT = "toc-macro-server-impl";
    protected static final String CLIENT_IMPL_WEBRESOURCE_CONTEXT = "toc-macro-client-impl";


    private final HtmlToXmlConverter htmlToXmlConverter;
    private final SettingsManager settingsManager;
    private final LocaleManager localeManager;
    private final I18NBeanFactory i18nBeanFactory;
    private final StaxDocumentOutlineCreator staxDocumentOutlineCreator;
    private final PageBuilderService pageBuilderService;

    public AbstractTOCMacro(StaxDocumentOutlineCreator staxDocumentOutlineCreator, HtmlToXmlConverter htmlToXmlConverter,
            SettingsManager settingsManager, LocaleManager localeManager, I18NBeanFactory i18nBeanFactory, final PageBuilderService pageBuilderService)
    {
        this.staxDocumentOutlineCreator = staxDocumentOutlineCreator;
        this.htmlToXmlConverter = htmlToXmlConverter;
        this.settingsManager = settingsManager;
        this.localeManager = localeManager;
        this.i18nBeanFactory = i18nBeanFactory;
        this.pageBuilderService = pageBuilderService;
    }

    /**
     * @return the name of the macro.
     */
    public String getName()
    {
        return "toc";
    }

    @Override
    public TokenType getTokenType(Map map, String body, RenderContext renderContext)
    {
        return TokenType.BLOCK;
    }

    protected String getDefaultSeparatorName()
    {
        return SeparatorType.BRACKET.toString();
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    /**
     * Returns the final XHTML output for the macro.
     * 
     * @param parameters The macro parameters.
     * @param body The macro body.
     * @param toc The rendered table of contents XHTML.
     * @return The XHTML output for the macro.
     */
    protected abstract String createOutput(Map<String, String> parameters, String body, String toc);

    /**
     * Creates a table of contents section based on the supplied parameters and page content.
     * 
     * @param outline document outline
     * @param parameters The macro parameters.
     * @param conversionContext conversion context
     * @return The table of contents.
     * @throws MacroExecutionException if there is a problem while creating the table of contents.
     */
    private String createTOC(final Map<String, String> parameters, final ConversionContext conversionContext, final DocumentOutline outline) throws MacroExecutionException
    {
        final OutlineRenderer outlineRenderer = getOutlineRenderer(parameters, conversionContext);
        final OutputHandler outputHandler = getOutputHandler(parameters);

        try
        {
            return outlineRenderer.render(outline, outputHandler);
        }
        catch (IOException e)
        {
            throw new MacroExecutionException(e);
        }
    }

    private OutlineRenderer getOutlineRenderer(final Map<String, String> parameters, final ConversionContext conversionContext)
            throws MacroExecutionException
    {
        String prefix = "";
        int minLevel = DEFAULT_MIN_LEVEL;
        int maxLevel = DEFAULT_MAX_LEVEL;

        try
        {
            minLevel = getMinLevel(parameters);
            maxLevel = getMaxLevel(parameters);
        }
        catch (Exception e)
        {
            log.warn(String.format("Invalid min/max level specified : %s , %s", isBlank(parameters.get(MIN_LEVEL_PARAM)) ? parameters.get(MIN_LEVEL_LOWCASE_PARAM) : Integer.parseInt(parameters.get(MIN_LEVEL_PARAM)), isBlank(parameters.get(MAX_LEVEL_PARAM)) ? parameters.get(MAX_LEVEL_PARAM) : parameters.get(MAX_LEVEL_LOWCASE_PARAM)), e);
        }

        if (minLevel > maxLevel)
            throw new MacroExecutionException(getI18nBean().getText("macro.error.minlevelgtmaxlevel", new Object[] { minLevel, maxLevel }));

        final String className = parameters.get(CLASS_PARAM);
        final String include = defaultIfEmpty(parameters.get(INCLUDE_PARAM), parameters.get(FILTER_PARAM));
        final String exclude = parameters.get(EXCLUDE_PARAM);

        if (Boolean.parseBoolean(parameters.get(ABSOLUTE_URL_PARAM)))
            prefix = settingsManager.getGlobalSettings().getBaseUrl() + conversionContext.getEntity().getUrlPath();

        final boolean outlineNumbering = BooleanUtils.toBoolean(parameters.get(OUTLINE_PARAM));
        return getOutlineRenderer(className, minLevel, maxLevel, prefix, include, exclude, outlineNumbering);
    }

    private OutputHandler getOutputHandler(final Map<String, String> parameters) throws MacroExecutionException
    {
        final String type = defaultIfEmpty(parameters.get(TYPE_PARAM), getDefaultType());
        final String separatorParam = defaultIfEmpty(parameters.get(SEPARATOR_PARAM), getDefaultSeparatorName());
        final String indent = parameters.get(INDENT_PARAM);
        final String style = defaultIfEmpty(parameters.get(STYLE_PARAM), DEFAULT_STYLE);

        if (FLAT_TYPE.equals(type))
            return new FlatHandler(separatorParam);
        else if (LIST_TYPE.equals(type))
            return  createListHandler(indent, style);
        else
            throw new MacroExecutionException(getI18nBean().getText("macro.error.unsupportedtype", new Object[] { type }));
    }

    protected ListHandler createListHandler(String indent, String style)
    {
        return new ListHandler(style, indent);
    }

    private static int getMaxLevel(Map<String, String> parameters)
    {
        return Integer.parseInt(
                defaultIfEmpty(
                        defaultIfEmpty(parameters.get(MAX_LEVEL_PARAM), parameters.get(MAX_LEVEL_LOWCASE_PARAM)), String.valueOf(DEFAULT_MAX_LEVEL))
        );
    }

    private static int getMinLevel(Map<String, String> parameters)
    {
        return Integer.parseInt(
                defaultIfEmpty(
                        defaultIfEmpty(parameters.get(MIN_LEVEL_PARAM), parameters.get(MIN_LEVEL_LOWCASE_PARAM)), String.valueOf(DEFAULT_MIN_LEVEL))
        );
    }

    private DocumentOutline generateDocumentOutline(final String xhtmlPageContent)
    {
        try
        {
            return staxDocumentOutlineCreator.getOutline(xhtmlPageContent);
        }
        catch (StaxOutlineBuilderException ex)
        {
            throw propagate(new MacroExecutionException(getI18nBean().getText("macro.error.pageparsefail")));
        }
        catch (Exception e)
        {
            throw propagate(new MacroExecutionException(e));
        }
    }

    /**
     * Implementation of the old Macro interface. Delegates straight through to the newer interface.
     */
    @Override
    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        try
        {
            //noinspection unchecked
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        }
        catch (MacroExecutionException e)
        {
            throw new MacroException(e);
        }
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        pageBuilderService.assembler().resources().requireContext(SERVER_IMPL_WEBRESOURCE_CONTEXT);

        final boolean printable = BooleanUtils.toBoolean(defaultString(parameters.get(PRINTABLE_PARAM), "true"));

        if (!printable && isPdfOrWordExport(conversionContext))
        {
            return getUnprintableHtml(body);
        }

        /* Check we're not already inside a TOC render to avoid an infinite loop */
        if (isBeingRendered())
        {
            return EMPTY;
        }
        else
        {
            try
            {
                setBeingRendered(true);
                final String toc = getTOC(parameters, body, conversionContext);
                return createOutput(parameters, body, toc);
            }
            finally
            {
                setBeingRendered(false);
            }
        }
    }

    private static boolean isPdfOrWordExport(ConversionContext conversionContext)
    {
        final String outputType = conversionContext.getOutputType();
        return PDF.equals(outputType) || WORD.equals(outputType);
    }


    protected boolean isBeingRendered()
    {
        /* We need to check at the ThreadLocal level (synonymous to a HTTP request level) due to TOC-48 */
        return Boolean.TRUE.equals(renderingThreadLocal.get());
    }

    protected void setBeingRendered(final boolean beingRendered)
    {
        renderingThreadLocal.set(beingRendered);
    }

    /* Defined as protected for test overrides */
    protected OutlineRenderer getOutlineRenderer(String className, int minLevel, int maxLevel, String prefix, String include, String exclude, boolean outlineNumbering)
    {
        return new OutlineRenderer(className, minLevel, maxLevel, prefix, include, exclude, outlineNumbering);
    }

    private String getTOC(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        try
        {
            final DocumentOutline documentOutline = getDocumentOutline(parameters, body, conversionContext);
            if (documentOutline == null)
            {
                return EMPTY;
            }

            return createTOC(parameters, conversionContext, documentOutline);
        }
        catch (RuntimeException ex)
        {
            propagateIfInstanceOf(ex.getCause(), MacroExecutionException.class);
            throw ex;
        }
    }

    /**
     * Gets the document outline to be rendered as the TOC. Gets the page content, tidies it up, then converts
     * it into an outline structure.
     *
     * Note: Generating this outline is really expensive. We should cache it instead of calculating it every time we
     * render, for example by storing it as JSON in Content Properties.
     */
    @Nullable
    private DocumentOutline getDocumentOutline(final Map<String, String> parameters, final String body, final ConversionContext conversionContext)
          throws MacroExecutionException
    {
        // Ensure generated identifiers contain the correct page name
        final boolean includeOriginalContext = !Boolean.parseBoolean(parameters.get(ABSOLUTE_URL_PARAM));
        final String pageContent = getContent(parameters, body, copy(conversionContext, includeOriginalContext));

        if (isBlank(pageContent))
            return null;

        // dynamic content executing during the render could mean we have invalid HTML now.
        // it should be tidied.
        final String xmlPageContent = htmlToXmlConverter.convert(pageContent);

        return generateDocumentOutline(xmlPageContent);
    }

    private static ConversionContext copy(ConversionContext conversionContext, boolean includeOriginalContext)
    {
        final PageContext pageContext = conversionContext.getPageContext();
        final PageContext originalContext = pageContext.getOriginalContext();
        final PageContext previousContext;
        if (pageContext != originalContext && includeOriginalContext)
        {
           previousContext = originalContext.getEntity().toPageContext();
        }
        else
        {
            previousContext = null;
        }
        /* TOC-137 : we need to pass in a new conversionContext to TOC */
        return new DefaultConversionContext(new PageContext(conversionContext.getEntity(), previousContext));
    }

    private I18NBean getI18nBean()
    {
        final ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        final Locale locale = localeManager.getLocale(user);
        return i18nBeanFactory.getI18NBean(locale);
    }

    /**
     * Returns the rendered page content from which headings will be extracted. This function can be overridden by
     * subclasses to modify the desired content.
     * 
     * @param parameters The macro parameters.
     * @param body The macro body.
     * @param conversionContext conversion context
     * @return The content string to be processed.
     */
    protected abstract String getContent(Map<String, String> parameters, String body, ConversionContext conversionContext);

    /**
     * Returns the default style for the TOC.
     * 
     * @return The default style.
     */
    protected abstract String getDefaultType();

    /**
     * Implementations should return the HTML to be shown if the macro parameter, <code>printable</code>, is set to
     * <code>false</code>
     * 
     * @return The HTML to be shown if the macro parameter, <code>printable</code>, is set to <code>false</code>
     * 
     * @param body The macro body.
     */
    protected abstract String getUnprintableHtml(String body);
}
