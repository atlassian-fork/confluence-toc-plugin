package net.customware.confluence.plugin.toc;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.customware.confluence.plugin.toc.DefaultDocumentOutlineBuilder.BuildableHeading;

import static org.apache.commons.lang.StringUtils.isNotBlank;

class DocumentOutlineImpl implements DocumentOutline
{
    /**
     * A matcher that will match everything <strong>except</strong> a placeholder, with a placeholder defined as a heading without a name.
     */
    static final HeadingMatcher NOT_PLACEHOLDER_MATCHER = new HeadingMatcher()
    {
        @Override
        public boolean matches(Heading heading)
        {
            return heading.getName() != null;
        }
    };

    private final List<BuildableHeading> topLevel;

    DocumentOutlineImpl(List<BuildableHeading> topLevelHeadings)
    {
        this.topLevel = topLevelHeadings;
    }

    @Override
    public Iterator<Heading> iterator()
    {
        return new ConstraintWrappingIterator(NOT_PLACEHOLDER_MATCHER, new AllIterator());
    }

    @Override
    public Iterator<Heading> iterator(int minLevel, int maxLevel, String includeRegex, String excludeRegex)
    {
        Iterator<Heading> iterator = new ConstraintWrappingIterator(new TypeRangeHeadingMatcher(minLevel, maxLevel), iterator());
        if (isNotBlank(includeRegex))
        {
            iterator = new ConstraintWrappingIterator(new RegexHeadingMatcher(includeRegex), iterator);
        }

        if (isNotBlank(excludeRegex))
        {
            iterator = new ConstraintWrappingIterator(new ExclusionRegexHeadingMatcher(excludeRegex), iterator);
        }

        return iterator;
    }

    /**
     * Implemented by a class that is used to find if a heading matches some particular criteria.
     */
    static interface HeadingMatcher
    {
        boolean matches(Heading heading);
    }

    /**
     * A HeadingMatcher which will return false for any headings that do not match a particular regex.
     */
    protected static class RegexHeadingMatcher implements HeadingMatcher
    {
        private Pattern pattern;

        public RegexHeadingMatcher(String pattern)
        {
            this.pattern = Pattern.compile(pattern);
        }

        @Override
        public boolean matches(Heading heading)
        {
            Matcher matcher = pattern.matcher(heading.getName());
            return matcher.matches();
        }
    }

    /**
     * A HeadingMatcher which will return false for any heading that <strong>do</strong> match a particular regex.
     */
    protected static class ExclusionRegexHeadingMatcher implements HeadingMatcher
    {
        private HeadingMatcher delegateMatcher;

        public ExclusionRegexHeadingMatcher(String pattern)
        {
            delegateMatcher = new RegexHeadingMatcher(pattern);
        }

        @Override
        public boolean matches(Heading heading)
        {
            return !delegateMatcher.matches(heading);
        }
    }

    /**
     * A HeadingMatcher that will check the type of a heading falls within a certain range (inclusive).
     */
    protected static class TypeRangeHeadingMatcher implements HeadingMatcher
    {
        private final int minType;
        private final int maxType;

        public TypeRangeHeadingMatcher(int minType, int maxType)
        {
            if (minType > maxType)
                throw new IllegalArgumentException("The minType must be the same or less than the maxType");

            this.minType = minType;
            this.maxType = maxType;
        }

        @Override
        public boolean matches(Heading heading)
        {
            int type = heading.getType();
            return type >= minType && type <= maxType;
        }
    }

    /**
     * A non thread safe iterator over all of the headings.
     */
    private class AllIterator implements Iterator<Heading>
    {
        /**
         * The current index for the children of the parent being iterated over.
         */
        private final Stack<Integer> childIndexes = new Stack<Integer>();

        /**
         * The last heading returned by a call on this iterator (hasNext or next).
         */
        private BuildableHeading currentHeading = null;

        /**
         * The next BuildableHeading to be returned by the Iterator.
         */
        private BuildableHeading nextHeading = null;

        public AllIterator()
        {
            childIndexes.push(-1); // no current index
        }

        @Override
        public boolean hasNext()
        {
            if (nextHeading != null)
                return true;

            nextHeading = getNext(currentHeading, childIndexes.peek() + 1);
            if (nextHeading == null)
                currentHeading = null;

            return nextHeading != null;
        }

        private BuildableHeading getNext(final BuildableHeading parent, int nextChildIndex)
        {
            if (parent == null)
            {
                // top level items
                if (nextChildIndex < topLevel.size())
                {
                    childIndexes.pop();
                    childIndexes.push(nextChildIndex); // increment the index for the current level
                    childIndexes.push(-1); // next call is for the first child of this new heading (a new level deeper)
                    return topLevel.get(nextChildIndex);
                }
                else
                {
                    // no parent so a top level element, but there are no more top level elements.
                    return null;
                }
            }

            if (parent.hasChildren() && (nextChildIndex < parent.getChildCount()))
            {
                // the current parent has children and we haven't iterated over them all yet
                childIndexes.pop();
                childIndexes.push(nextChildIndex);
                childIndexes.push(-1); // next call is for the first child of this new heading (a new level deeper)
                return parent.getChild(nextChildIndex);
            }

            // we have exhausted all the children for this parent, or it didn't have any. Go up a level to the next
            // sibling
            childIndexes.pop(); // discard the index for this level
            return getNext(parent.getParent(), childIndexes.peek() + 1);
        }

        @Override
        public Heading next()
        {
            if (!hasNext())
                throw new NoSuchElementException();

            BuildableHeading heading = nextHeading;
            currentHeading = nextHeading;
            nextHeading = null; // so that the next call needs to find a new nextHeading
            return heading;
        }

        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * An iterator that wraps another iterator and ensures that the only Heading objects returned by the wrapped
     * iterator are those that satisfy the supplied HeadingMatcher
     */
    private static class ConstraintWrappingIterator implements Iterator<Heading>
    {
        private final Iterator<Heading> wrappedIterator;
        private final HeadingMatcher matcher;
        private Heading next = null;

        public ConstraintWrappingIterator(HeadingMatcher matcher, Iterator<Heading> iterator)
        {
            this.wrappedIterator = iterator;
            this.matcher = matcher;
        }

        @Override
        public boolean hasNext()
        {
            if (next != null)
                return true;

            if (!wrappedIterator.hasNext())
                return false;

            Heading candidateHeading = wrappedIterator.next();

            while (!matcher.matches(candidateHeading))
            {
                if (wrappedIterator.hasNext())
                {
                    candidateHeading = wrappedIterator.next();
                }
                else
                {
                    candidateHeading = null;
                    break;
                }
            }

            next = candidateHeading;
            return next != null;
        }

        @Override
        public Heading next()
        {
            if (!hasNext())
                throw new NoSuchElementException();

            Heading forReturn = next;
            next = null;
            return forReturn;
        }

        @Override
        public void remove()
        {
            wrappedIterator.remove();
        }
    }
}
