package net.customware.confluence.plugin.toc;

import java.util.Iterator;

import javax.xml.stream.XMLOutputFactory;

import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.DelegateXmlOutputFactory;

import com.ctc.wstx.api.WstxOutputProperties;

import org.junit.Before;
import org.junit.Test;

import net.customware.confluence.plugin.toc.DocumentOutline.Heading;

import static org.junit.Assert.assertEquals;

public class StaxDocumentOutlineCreatorTest
{
    private StaxDocumentOutlineCreator staxBuilder;
    
    @Before
    public void setUp() throws Exception
    {
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        xmlOutputFactory.setProperty(WstxOutputProperties.P_OUTPUT_VALIDATE_STRUCTURE, Boolean.FALSE);
        xmlOutputFactory.setProperty(WstxOutputProperties.P_AUTOMATIC_END_ELEMENTS, Boolean.FALSE);

        staxBuilder = new StaxDocumentOutlineCreator(new DefaultXmlEventReaderFactory(), new DelegateXmlOutputFactory(xmlOutputFactory));
    }

    @Test
    public void testMissingTopHeadings() throws Exception
    {
        Iterator<Heading> iterator = staxBuilder.getOutline("<body><p>nothing</p><h3 id=\"anchor1\">Heading 3</h3><h2 id=\"anchor2\">Heading 2</h2><p>nothing</p><h3 id=\"anchor3\">SubHeading 3</h3></body>").iterator();

        Heading heading = iterator.next();
        assertEquals("Heading 3", heading.getName());
        assertEquals("anchor1", heading.getAnchor());
        assertEquals(2, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("Heading 2", heading.getName());
        assertEquals("anchor2", heading.getAnchor());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(1, heading.getChildCount());

        heading = iterator.next();
        assertEquals("SubHeading 3", heading.getName());
        assertEquals("anchor3", heading.getAnchor());
        assertEquals(2, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());
    }

    @Test
    public void testOnlyOneLevelOfHeadings() throws Exception
    {
        Iterator<Heading> iterator = staxBuilder.getOutline("<body><p>nothing</p><h1>a</h1><p>stuff</p><H1>b</H1><H1>c</H1></body>").iterator();

        Heading heading = iterator.next();
        assertEquals("a", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("b", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("c", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());
    }

    @Test
    public void testABigJumbleOfHeadings() throws Exception
    {
        Iterator<Heading> iterator = staxBuilder.getOutline("<body><h1>Level-1-Heading-1</h1><h2>Level-2-Heading-1</h2><h3>Level-3-Heading-<b>1</b></h3><h2>Level-2-Heading-2</h2><h5>Level-5-Heading-1</h5><h6 id='TOCtest-Level6Heading1'>Level-6-Heading-1</h6><h4>Level-4-Heading-1</h4><h2>Level-2-Heading-3</h2></body>").iterator();

        Heading heading = iterator.next();
        assertEquals("Level-1-Heading-1", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());

        heading = iterator.next();
        assertEquals("Level-2-Heading-1", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());

        heading = iterator.next();
        assertEquals("Level-3-Heading-1", heading.getName());
        assertEquals(3, heading.getEffectiveLevel());

        heading = iterator.next();
        assertEquals("Level-2-Heading-2", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());

        heading = iterator.next();
        assertEquals("Level-5-Heading-1", heading.getName());
        assertEquals(4, heading.getEffectiveLevel());

        heading = iterator.next();
        assertEquals("Level-6-Heading-1", heading.getName());
        assertEquals(5, heading.getEffectiveLevel());

        heading = iterator.next();
        assertEquals("Level-4-Heading-1", heading.getName());
        assertEquals(3, heading.getEffectiveLevel());

        heading = iterator.next();
        assertEquals("Level-2-Heading-3", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());
    }

    @Test
    public void testAnchorSupport() throws Exception
    {
        Iterator<Heading> iterator = staxBuilder.getOutline("<body><h2><a href=\"#\">Header</a> <i>with</i><a name='anchor1'> <b>two</b></a> <a name=\"anchor2\">anchors</a></h2></body>").iterator();
        Heading heading = iterator.next();
        
        assertEquals("Header with two anchors", heading.getName());
        assertEquals("anchor1", heading.getAnchor());
    }

    @Test
    public void testNumberedHeadingsWithAnchors() throws Exception
    {
        /*
         *  <h1 id="numberedheadings-Heading-1heading1">
         *      <span class="nh-number">1. </span>
         *      Heading-1
         *      <span class="confluence-anchor-link" id="numberedheadings-heading1"></span>
         *  </h1>
         */
        Iterator<Heading> iterator = staxBuilder.getOutline("<body><h1 id=\"numberedheadings-Heading-1heading1\"><span class=\"nh-number\">1. </span>Heading-1<span class=\"confluence-anchor-link\" id=\"numberedheadings-heading1\"></span></h1></body>").iterator();

        Heading heading = iterator.next();
        assertEquals("1. Heading-1", heading.getName());
        assertEquals("numberedheadings-Heading-1heading1", heading.getAnchor());

    }

    @Test
    public void testHeadingsStartWithAnchors() throws Exception
    {
        /*
         * <h1 id="heading-h1Heading-1">
         *     <span class="confluence-anchor-link" id="heading-h1"></span>
         *     Heading-1
         *  </h1>
         */
        Iterator<Heading> iterator = staxBuilder.getOutline("<body><h1 id=\"heading-h1Heading-1\"><span class=\"confluence-anchor-link\" id=\"heading-h1\"></span>Heading-1</h1></body>").iterator();

        Heading heading = iterator.next();
        assertEquals("Heading-1", heading.getName());
        assertEquals("heading-h1Heading-1", heading.getAnchor());
    }

    @Test
    public void testHeadingsEndWithAnchors() throws Exception
    {
        /*
         * <h1 id="heading-Heading-1heading1">
         *     Heading-1&nbsp;
         *     <span class="confluence-anchor-link" id="heading-heading1"></span>
         * </h1>
         */
        Iterator<Heading> iterator = staxBuilder.getOutline("<body><h1 id=\"heading-Heading-1heading1\">Heading-1<span class=\"confluence-anchor-link\" id=\"heading-heading1\"></span></h1></body>").iterator();

        Heading heading = iterator.next();
        assertEquals("Heading-1", heading.getName());
        assertEquals("heading-Heading-1heading1", heading.getAnchor());
    }

    @Test
    public void testColorHeadings() throws Exception
    {
         /*
         *  <h1 id="colorheadings-Heading1withtext">
         *      <span style="color: rgb(255,0,0);">Heading 1</span>
         *      with text
         *  </h1>
         */
        Iterator<Heading> iterator = staxBuilder.getOutline("<body><h1 id=\"colorheadings-Heading1withtext\"><span style=\"color: rgb(255,0,0);\">Heading 1</span> with text</h1></body>").iterator();

        Heading heading = iterator.next();
        assertEquals("Heading 1 with text", heading.getName());
        assertEquals("colorheadings-Heading1withtext", heading.getAnchor());
    }

}
