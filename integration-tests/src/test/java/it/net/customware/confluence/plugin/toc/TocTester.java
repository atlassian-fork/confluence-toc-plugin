package it.net.customware.confluence.plugin.toc;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.helper.HelperFactory;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import net.sourceforge.jwebunit.junit.WebTester;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

class TocTester
{
    private final WebTester webTester;

    protected TocTester(final WebTester webTester)
    {
        this.webTester = webTester;
    }

    String createTestSpace()
    {
        final SpaceHelper spaceHelper = HelperFactory.createSpaceHelper((ConfluenceWebTester) webTester);

        String testSpaceKey = "tst" + randomNumeric(10);
        spaceHelper.setKey(testSpaceKey);
        spaceHelper.setName("Test Space");
        spaceHelper.setDescription("Test Space");

        assertTrue(spaceHelper.create());

        return testSpaceKey;
    }


    public void verifyHeadings(String tocListXPath, List<Heading> headings)
    {
        webTester.assertElementNotPresentByXPath(tocListXPath + "/li[" + (headings.size() + 1) + "]");

        for (int i = 0; i < headings.size(); ++i)
        {
            final String headingXPath = tocListXPath + "/li[" + (i + 1) + "]";
            final Heading heading = headings.get(i);

            if (null == heading.href)
            {
                final String expectedText = isBlank(heading.outline) ? heading.text : heading.outline + ' ' + heading.text;
                assertEquals(
                        "Unexpected tex for heading " + i,
                        expectedText,
                        webTester.getElementTextByXPath(headingXPath));
            }
            else
            {
                final String headingLinkXpath = headingXPath + "/a";

                assertEquals("Unexpected text for heading " + i,
                        heading.text, webTester.getElementTextByXPath(headingLinkXpath));

                assertEquals("Unexpected href attribute for heading " + i,
                        heading.href, webTester.getElementAttributeByXPath(headingLinkXpath, "href"));
            }

            if (null != heading.outline)
            {
                assertEquals(heading.outline, webTester.getElementTextByXPath(headingXPath + "/span[@class='TOCOutline']"));
            }

            if (heading.hasChildren())
            {
                verifyHeadings(
                        headingXPath + "/a/following-sibling::ul[li]",
                        heading.children);
            }
        }
    }

    public void verifyFlatHeadings(String tocListXPath, List<Heading> headings)
    {
        int i = 0;
        webTester.assertElementNotPresentByXPath(tocListXPath + "/a[" + (headings.size() + 1) + "]");

        for (Heading heading : headings)
        {
            final String headingXPath = "(" + tocListXPath + "//a)[" + ++i + "]";

            assertEquals(heading.text, webTester.getElementTextByXPath(headingXPath));
            assertEquals(heading.href, webTester.getElementAttributeByXPath(headingXPath, "href"));

            if (isNotBlank(heading.outline))
            {
                assertEquals(heading.outline, webTester.getElementTextByXPath(tocListXPath + "/span[@class='TOCOutline'][" + i + "]"));
            }
        }
    }
}
